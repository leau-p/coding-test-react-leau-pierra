import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';

import { userService } from 'services';
import { parseDate } from 'utils';


class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: null,
            user: {}
        }

        this.handleChange = this.handleChange.bind(this);
        this.getSubPage = this.getSubPage.bind(this);
    }

    componentDidMount() {
      let newId;
      const subPage = this.getSubPage()

      if (subPage) {
        newId = Number(subPage)
      }
      
      // if no subpage, the id is the first of the results
      userService.list().then((result) => {
          this.setState((prevState) => ({...prevState, list: result, id: newId || result[0].id}))
        }).catch((error) => console.error(error))
    }

    componentDidUpdate(prevProps, prevState) {
      const { id, list } = this.state
      const subPage = this.getSubPage()
      const defaultId = list[0].id


      // subpage changed
      if (this.getSubPage(prevProps.location) !== subPage) {
        const newId = subPage ? Number(subPage) : defaultId
        return this.setState((prevState) => ({...prevState, id: newId}))
      }

      // id did not change
      if (id === prevState.id) return

      // on initial id assignment, do not change URL
      if ( subPage || id !== defaultId) {
        this.props.history.push(`/users/${id}`)
      }

      userService.get(id).then((result) => {
        this.setState((prevState) => ({...prevState, user: result}))
      }).catch((error) => console.error(error)) 
    }

    getSubPage(currentLocation) {
      return (currentLocation || this.props.location).pathname.split("/")[2]
    }

    handleChange(event) {
      this.setState({ id: event.target.value });
    }

    render() {
      const { list, id, user } = this.state;

      return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <Link to="/" className="nav-arrow">
                        <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                    </Link>

                  <div className="content-block">
                    <div className="users-select">
                        <h1>
                            <select value={id || 1} onChange={this.handleChange}>
                                {list.map((user) => <option value={user.id} key={user.id}>{user.name.match(/[a-zA-Z ]+/)[0]}</option>)}
                            </select>
                        </h1>
                    </div>

                    <div className="infos-block">
                        {user.occupation && <div>Occupation : <span className="info-value">{user.occupation}</span></div>}
                        {user.birthdate && <div>Date de naissance : <span className="info-value">{parseDate(user.birthdate)}</span></div>}
                    </div>

                    <div className="articles-list">
                        {user.articles && user.articles.map((article) => (
                          <article className="article-block" key={article.id}>
                            <h2>{article.name}</h2>
                            <p>{article.content}</p>
                          </article>
                        ))}
                    </div>
                  </div>
                </div>
            </Fragment>
        )
    }
}

export default UserPage;
