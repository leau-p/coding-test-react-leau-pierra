const months = [
  "janvier",
  "février",
  "mars",
  "avril",
  "mai",
  "juin",
  "juillet",
  "août",
  "septembre",
  "octobre",
  "novembre",
  "décembre"
]

export const parseDate = (dateString) => {
  const date = new Date(dateString)
  let dateNum = date.getUTCDate()
  if (dateNum === 1) dateNum += "er"

  return `${dateNum} ${months[date.getUTCMonth()]} ${date.getUTCFullYear()}`
}